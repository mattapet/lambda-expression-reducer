//
//  Parser.cpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/19/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#include "Parser.hpp"
#include "../Basic/Exception.hpp"

using namespace lambda;

Parser::Parser(std::istream &input)
: lexer(std::make_unique<Lexer>(input)), depth(0)
{}

std::shared_ptr<ASTNode> Parser::parse() {
    auto ast = parseTerm();
    if (lexer->hasValue()) {
        auto tok = lexer->peek();
        throw UnexpectedSymbolException(tok->text(), tok->range().start);
    }
    return ast;
}

// MARK: - Private implementation

void Parser::consumeToken() {
    if (lexer->hasValue()) {
        lexer->next();
    }
}

void Parser::consumeLParen() {
    depth += 1;
    consumeToken();
}

void Parser::consumeRParen() {
    depth -= 1;
    if (depth < 0) {
        throw ParenthesizationMismatch();
    }
    consumeToken();
}


std::shared_ptr<ASTNode> Parser::parseTerm() {
    if (lexer->hasBack() && lexer->peekBack()->isLParen() &&
        consumeToken(TokenType::Keyword)) {
        auto arg = lexer->peek()->text();
        /* Declare variable */
        consumeToken();
        auto tok = lexer->peek();
        if (!consumeToken(TokenType::Arg_Seprator)) {
            throw UnexpectedSymbolException(tok->text(), tok->range().start);
        }
        return std::make_shared<Function>(arg, parseTerm());
    } else {
        return parseApp();
    }
}

std::shared_ptr<ASTNode> Parser::parseApp() {
    std::shared_ptr<ASTNode> lhs = parseAtom();
    for (;;) {
        std::shared_ptr<ASTNode> rhs = parseAtom();
        if (rhs && lhs) {
            lhs = std::make_shared<Application>(lhs, rhs);
        } else {
            return lhs;
        }
    }
}

std::shared_ptr<ASTNode> Parser::parseAtom() {
    if (!lexer->hasValue() || lexer->peek()->isRParen()) {
        return nullptr;
    }
    if (consumeToken(TokenType::L_Paren)) {
        auto term = parseTerm();
        if (!consumeToken(TokenType::R_Paren)) {
            throw ParenthesizationMismatch();
        }
        return term;
    }
    auto tok = lexer->peek();
    if (tok->isIdentifier()) {
        auto tok = lexer->peek();
        consumeToken();
        /* Set variable reference */
        return std::make_shared<Variable>(tok->text());
        
    } else if (tok->is(TokenType::BoolLiteral)) {
        return parseBoolLiteral();
    } else if (tok->is(TokenType::NumberLiteral)) {
        return parseNumberLiteral();
    } else {
        throw UnexpectedSymbolException(tok->text(), tok->range().start);
    }
}

std::shared_ptr<ASTNode> Parser::parseBoolLiteral() {
    auto tok = lexer->peek();
    consumeToken();
    auto var = std::make_shared<Variable>("y");
    if (tok->text() == "true") {
        var->identifier("x");
    }
    return std::make_shared<Function>(
        "x", std::make_shared<Function>("y", var)
    );
}

std::shared_ptr<ASTNode> Parser::parseNumberLiteral() {
    auto tok = lexer->peek();
    auto value = std::stoi(tok->text());
    consumeToken();
    std::shared_ptr<ASTNode> ret = std::make_shared<Variable>("x");
    for (int i = 0; i < value; i++) {
        ret = std::make_shared<Application>(
            std::make_shared<Variable>("f"),
            ret
        );
    }
    ret = std::make_shared<Function>("x", ret);
    return std::make_shared<Function>("f", ret);
}

