//
//  Token.cpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/20/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#include "Token.hpp"

using namespace lambda;

Token::Token(TokenType t, const SourceRange &range, const std::string &text)
: _type(t),
  _range(range),
  _text(text)
{}
