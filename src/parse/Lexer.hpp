//
//  Lexer.hpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/20/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#ifndef Lexer_hpp
#define Lexer_hpp

#include "Token.hpp"

#include <vector>
#include <iostream>
#include <string>
#include <memory>
#include <unordered_map>
#include <cassert>

namespace lambda {
    
    /// \brief Object that tokenizes an input stream for the \c Parser and
    ///  serves as an interface to access and interate over them.
    class Lexer {
        std::istream &input;
        std::size_t _position;
        std::vector<std::shared_ptr<Token>> _tokens;
        bool _numberLiteral = false;
        
    public:
        
        // MARK: - Lifecycle of Lexer objects.
        
        Lexer(std::istream &input);
        
        ~Lexer() = default;
        
        // MARK: - Accessing tokens
        
        /// Returns next token.
        std::shared_ptr<Token> peekNext() const;
        /// Returns current token.
        std::shared_ptr<Token> peek() const;
        /// Returns previous token.
        std::shared_ptr<Token> peekBack() const;
        /// Retruns token at given position.
        std::shared_ptr<Token> getToken(std::size_t position) const;
        
        
        // MARK: - Token position managements
        
        /// Sets lexing position.
        void setPosition(std::size_t position);
        /// Gets next lexing position.
        void next();
        /// Gets previous lexing position.
        void back();
        /// Resets lexing position.
        void reset();
        
        // MARK: - Accessing position properties
        
        /// Returns current location in the source buffer.
        const SourceRange &location() const;
        /// Returns lexing position.
        std::size_t tokenPosition() const;
        
        /// \return \c true if a lexer has currenly a value, otherwise \c false.
        bool hasValue() const;
        /// \return \c true if a lexer has a next value, otherwise \c false.
        bool hasNext() const;
        /// \return \c true if a lexer has a previous value, otherwise \c false.
        bool hasBack() const;
        
    private:
        
        // MARK: - Lexing methods.
        
        /// Lexes input.
        void lex();
        /// Pushes a token identifier to lexed tokens.
        void pushIdentifier(std::string &str, SourceRange range);
        /// Pushes a token to lexed tokens.
        void pushToken(TokenType type, std::string text, SourceRange range);
    };
    
}

#endif /* Lexer_hpp */
