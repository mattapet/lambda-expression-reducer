//
//  Lexer.cpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/20/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#include "Lexer.hpp"
#include "../Basic/Exception.hpp"
#include <sstream>

using namespace lambda;

Lexer::Lexer(std::istream &input)
: input(input), _position(0) {
    lex();
}

// MARK: - Accessing tokens

std::shared_ptr<Token> Lexer::peekNext() const {
    assert(_position + 1 < _tokens.size() && "Token out of bounds.");
    return _tokens[_position + 1];
}

std::shared_ptr<Token> Lexer::peek() const {
    assert(_position < _tokens.size() && "Token out of bounds.");
    return _tokens[_position];
}

std::shared_ptr<Token> Lexer::peekBack() const {
    assert(_position - 1 < _tokens.size() && "Token out of bounds.");
    return _tokens[_position - 1];
}

std::shared_ptr<Token> Lexer::getToken(std::size_t position) const {
    assert(position + 1 < _tokens.size() && "Token out of bounds.");
    return _tokens[position];
}

// MARK: - Token position managements

void Lexer::setPosition(std::size_t position) {
    assert(position < _tokens.size() && "Token out of bounds.");
    _position = position;
}

void Lexer::next() {
    _position += 1;
}

void Lexer::back() {
    _position -= 1;
}

void Lexer::reset() {
    _position = 0;
}


// MARK: - Accessing position properties
const SourceRange &Lexer::location() const {
    return _tokens[_position]->range();
}

std::size_t Lexer::tokenPosition() const {
    return _position;
}

bool Lexer::hasValue() const {
    return _position < _tokens.size();
}

bool Lexer::hasNext() const {
    return _position + 1 < _tokens.size();
}

bool Lexer::hasBack() const {
    return _position - 1 < _tokens.size();
}


// MARK: - Lexing methods.

void Lexer::lex() {
    char c;
    std::size_t row = 1, col = 1;
    std::size_t startRow = row, startCol = col;
    std::string id = "";
    bool inComment = false;
    
    while (input) {
        input >> std::noskipws >> c;
        
        if (inComment) {
            if (c == '\n') {
                inComment = false;
                row += 1;
                col = 1;
            }
            continue;
        }
        
        switch (c) {
            case '#':
                inComment = true;
                break;
                
            case '(':
                pushIdentifier(
                    id,
                    SourceRange{{startRow, startCol},{row, col}}
                );
                pushToken(
                    TokenType::L_Paren,
                    std::string(1, c),
                    SourceRange{{row, col},{row, col}}
                );
                break;
                
            case ')':
                pushIdentifier(
                    id,
                    SourceRange{{startRow, startCol},{row, col}}
                );
                pushToken(
                    TokenType::R_Paren,
                    std::string(1, c),
                    SourceRange{{row, col},{row, col}}
                );
                break;
                
            case '.':
                pushIdentifier(
                    id,
                    SourceRange{{startRow, startCol},{row, col}}
                );
                pushToken(
                    TokenType::Arg_Seprator,
                    std::string(1, c),
                    SourceRange{{row, col},{row, col}}
                );
                break;
                
            case '\\':
                pushIdentifier(
                    id,
                    SourceRange{{startRow, startCol},{row, col}}
                );
                pushToken(
                    TokenType::Keyword,
                    "lambda",
                    SourceRange{{row, col},{row, col}}
                );
                break;
                
            default:
                if (isspace(c)) {
                    auto range = SourceRange{{startRow, startCol},{row, col}};
                    pushIdentifier(id, range);
                    break;
                    
                } else if (isalpha(c)) {
                    if (_numberLiteral) {
                        throw UnexpectedSymbolException(c, {row, col});
                    }
                    
                    if (!id.length()) {
                        startRow = row;
                        startCol = col;
                    }
                    id += std::string(1, c);
                
                } else if (isnumber(c)) {
                    if (!id.length()) {
                        _numberLiteral = true;
                    }
                    id += std::string(1, c);
                    
                } else {
                    throw UnexpectedSymbolException(c, {row, col});
                }
        }
        
        if (c == '\n') {
            row += 1;
            col = 1;
        } else {
            col += 1;
        }
    }
}


void Lexer::pushIdentifier(std::string &str, lambda::SourceRange range) {
    if (str.length()) {
        if (_numberLiteral) {
            _numberLiteral = false;
            pushToken(TokenType::NumberLiteral, str, range);
        } else if (str == "lambda") {
            pushToken(TokenType::Keyword, str, range);
        } else if (str[0] == 't' && str == "true") {
            pushToken(TokenType::BoolLiteral, str, range);
        } else if (str[0] == 'f' && str == "false") {
            pushToken(TokenType::BoolLiteral, str, range);
        } else {
            pushToken(TokenType::Identifier, str, range);
        }
        str = "";
    }
}

void Lexer::pushToken(TokenType type, std::string text, SourceRange range) {
    _tokens.push_back(std::make_shared<Token>(type, range, text));
}





