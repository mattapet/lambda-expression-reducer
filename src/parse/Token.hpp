//
//  Token.hpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/20/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#ifndef Token_hpp
#define Token_hpp

#include "Basic/SourceLocation.hpp"

#include <string>
#include <exception>

namespace lambda {

    /// \brief Type of a token.
    enum TokenType {
        Keyword,
        Identifier,
        L_Paren,
        R_Paren,
        Arg_Seprator,
        NumberLiteral,
        BoolLiteral
    };
    
    /// \brief Encapsulation of a single token with the source text and source
    ///  range. It provides unifier interface to handle, differentiate and work
    ///  with tokens produces by the \c Lexer. A tokens are immutable objects
    ///  used only as an input for the \c Parser to create an \c AST.
    class Token {
        TokenType _type;
        const SourceRange _range;
        const std::string _text;
        
    public:
        Token(TokenType t, const SourceRange &range, const std::string &text);
        ~Token() = default;
        
        // MARK: - Type comparators.
        
        bool is(TokenType type) const {
            return _type == type;
        }
        
        bool isNot(TokenType type) const {
            return _type != type;
        }
        
//        bool isAny(TokenType type) {
//            return is(type);
//        }
//
//        template<typename ...T>
//        bool isAny(TokenType type1, TokenType type2, T... rest) const {
//            if (is(type1)) {
//                return true;
//            }
//            return isAny(type2, rest...);
//        }
//
//        template<typename ...T>
//        bool isNot(TokenType type, T... rest) const {
//            return !isAny(type, rest...);
//        }
        
        // MARK: - Type predicates
        
        bool isAnyParen() const {
            return isLParen() || isRParen();
        }
        
        bool isLParen() const {
            return is(TokenType::L_Paren);
        }
        
        bool isRParen() const {
            return is(TokenType::R_Paren);
        }
        
        bool isKeyword() const {
            return is(TokenType::Keyword);
        }
        
        bool isIdentifier() const {
            return is(TokenType::Identifier);
        }
        
        bool isLiteral() const {
            return is(TokenType::NumberLiteral) || is(TokenType::BoolLiteral);
//            return isAny(TokenType::NumberLiteral, TokenType::BoolLiteral);
        }
        
        // MARK: - Accessing token properties
        
        const SourceRange &range() const {
            return _range;
        }
        
        const std::string &text() const {
            return _text;
        }
        
        std::size_t length() const {
            return _text.length();
        }
        
        TokenType type() const {
            return _type;
        }
    };
}

#endif /* Token_hpp */
