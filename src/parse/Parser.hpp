//
//  Parser.hpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/19/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#ifndef Parser_hpp
#define Parser_hpp

#include "AST/ASTNodes.hpp"
#include "Token.hpp"
#include "Lexer.hpp"

#include <iostream>
#include <string>
#include <memory>

namespace lambda {
    
    /// \brief \c Parser objects are used to parse provided input into a
    ///  lambda AST. It's a recursive descent parser, that uses a \c Lexer
    ///  to tokenize the input and then generates approprate AST out of it.
    class Parser {
        std::unique_ptr<Lexer> lexer;
        int depth;
        
    public:
        Parser(std::istream &input);
        
        /// Parses a input and returns resulting AST.
        std::shared_ptr<ASTNode> parse();
        
    private:
        
        /// Consumes a current token.
        void consumeToken();
        /// Consumes a left parenthese token.
        void consumeLParen();
        /// Consumes a right parenthese token.
        void consumeRParen();
        
        /// \brief Consumes a token of the given type.
        /// \return \c true, if consumtion of the token was successful, \c false
        ///  otherwise.
        bool consumeToken(TokenType tok) {
            if (lexer->hasValue() && lexer->peek()->is(tok)) {
                lexer->next();
                return true;
            }
            return false;
        }
        
        /// Parses a lambda term.
        std::shared_ptr<ASTNode> parseTerm();
        
        /// Parses a lambda application.
        std::shared_ptr<ASTNode> parseApp();
        
        /// Parses a lambda atom.
        std::shared_ptr<ASTNode> parseAtom();
        
        /// Parses a boolean literal atom.
        std::shared_ptr<ASTNode> parseBoolLiteral();
        
        /// Parses a number literal atom.
        std::shared_ptr<ASTNode> parseNumberLiteral();
        
    };
}

#endif /* Parser_hpp */
