//
//  Application.hpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/19/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#ifndef Application_hpp
#define Application_hpp

#include "ASTNode.hpp"

#include <string>
#include <memory>

namespace lambda {
    
    /// \brief Encapsulation of a lambda application of exactly two terms.
    ///  The application must always contain both, \c left and \c right
    ///  term. Should it not have a \c right child, the application node should
    ///  be left out.
    class Application: public ASTNode {
        std::shared_ptr<ASTNode> _left;
        std::shared_ptr<ASTNode> _right;
        
    public:
        Application(std::shared_ptr<ASTNode> left,
                    std::shared_ptr<ASTNode> right);
        
        /// Return the \c left node of the application.
        std::shared_ptr<ASTNode> left() {
            return _left;
        }
        /// Return the \c right node of the application.
        std::shared_ptr<ASTNode> right() {
            return _right;
        }
        
        // MARK: - Abstract AST methods.
        
        virtual std::shared_ptr<ASTNode> clone() override;
        
        virtual void walk(ASTWalker *walker) override;
        
        virtual std::shared_ptr<ASTNode>
        eval(std::shared_ptr<Scope> scope) override;
        
        virtual std::shared_ptr<ASTNode>
        substitute(std::size_t id, std::shared_ptr<ASTNode> value) override;
    };
}

#endif /* Application_hpp */
