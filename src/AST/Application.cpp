//
//  Application.cpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/19/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#include "Application.hpp"
#include <cassert>

using namespace lambda;

Application::Application(std::shared_ptr<ASTNode> left,
                         std::shared_ptr<ASTNode> right)
: _left(left),
  _right(right) {
    _left->ASTNode::parent(this);
    _right->ASTNode::parent(this);
}

std::shared_ptr<ASTNode> Application::clone() {
    return std::make_shared<Application>(_left->clone(), _right->clone());
}
