 //
//  Variable.cpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/19/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#include "Variable.hpp"

using namespace lambda;

Variable::Variable(const std::string &identifier, std::size_t id)
: _identifier(identifier),
  _id(id)
{}

std::shared_ptr<ASTNode> Variable::clone() {
    return std::make_shared<Variable>(_identifier, _id);
}
