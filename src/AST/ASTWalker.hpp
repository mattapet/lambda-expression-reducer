//
//  ASTWalker.hpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/28/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#ifndef ASTWalker_hpp
#define ASTWalker_hpp

#include "ASTNodes.hpp"

namespace lambda {
    
    
    class ASTWalker {
    protected:
        virtual void didEnter(Application *app) { }
        virtual void willLeave(Application *app) { }
        
        virtual void didEnter(Function *func) { }
        virtual void willLeave(Function *func) { }
        
        virtual void didEnter(Variable *var) { }
        virtual void willLeave(Variable *var) { }
        
    public:
        virtual void walk(Application *app);
        virtual void walk(Function *func);
        virtual void walk(Variable *var);
    };
    
    
}

#endif /* ASTWalker_hpp */
