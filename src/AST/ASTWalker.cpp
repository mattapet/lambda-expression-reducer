//
//  ASTWalker.cpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/28/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#include "ASTWalker.hpp"

using namespace lambda;

void ASTWalker::walk(Application *app) {
    didEnter(app);
    app->left()->walk(this);
    app->right()->walk(this);
    willLeave(app);
}

void ASTWalker::walk(Function *func) {
    didEnter(func);
    func->body()->walk(this);
    willLeave(func);
}

void ASTWalker::walk(Variable *var) {
    didEnter(var);
    var->identifier();
    willLeave(var);
}

// MARK: - ASTNode walk methods imlementations.

void Application::walk(ASTWalker *walker) {
    walker->walk(this);
}

void Function::walk(ASTWalker *walker) {
    walker->walk(this);
}

void Variable::walk(ASTWalker *walker) {
    walker->walk(this);
}
