//
//  Function.cpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/19/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#include "Function.hpp"

using namespace lambda;

Function::Function(std::string argument,
                   std::shared_ptr<ASTNode> body,
                   std::size_t id)
: _argument(argument),
  _body(body),
  _id(id) {
    _body->parent(this);
}

std::shared_ptr<ASTNode> Function::clone() {
    return std::make_shared<Function>(_argument, _body->clone(), _id);
}
