//
//  ASTNode.hpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/30/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#ifndef ASTNode_hpp
#define ASTNode_hpp

#include "../Runtime/Scope.hpp"

#include <memory>

namespace lambda {
    
    class ASTWalker;
    
    /// \brief Abstract base class for all derived classes, that wish to be
    ///  used to construct the \c AST. This class also declares a unified
    //// interface that is used by other base classes operating on the AST,
    ///  such as the \c ASTWalker.
    class ASTNode {
        ASTNode *_parent = nullptr;
        
    public:
        /// Constructs a basic AST node.
        ASTNode() = default;
        
        /// Destructs an AST node.
        virtual ~ASTNode() = default;
        
        // MARK: - Accessing methods.
        
        /// Returns a pointer to the parent of the node in the AST or \c nullptr
        /// if the node does not have a parent.
        const ASTNode *parent() const {
            return _parent;
        }
        
        /// Returns a pointer to the parent of the node in the AST or \c nullptr
        /// if the node does not have a parent.
        ASTNode *parent() {
            return _parent;
        }
        /// Sets node's parent to the given value.
        void parent(ASTNode *parent) {
            _parent = parent;
        }
        
        // MARK: - Abstract AST methods.
        
        /// A method that should create a deepy copy of the node and return it.
        virtual std::shared_ptr<ASTNode> clone() = 0;
        
        /// A method that is used by instances of \c ASTWalker class, to walk
        /// the AST tree.
        virtual void walk(ASTWalker *walker) = 0;
        
        /// Performs an evaluation step on the node and returns the result of
        /// the evaluation, while not changing the whole AST subtree.
        virtual std::shared_ptr<ASTNode> eval(std::shared_ptr<Scope> scope) = 0;
        
        /// Perform a substitution step of the evaluation process, while
        /// returning a new subtree without mutating the existing one.
        virtual std::shared_ptr<ASTNode>
        substitute(std::size_t id, std::shared_ptr<ASTNode> value) = 0;
    };
    
}

#endif /* ASTNode_hpp */
