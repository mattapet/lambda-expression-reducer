//
//  ASTNodes.hpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/21/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#ifndef ASTNodes_hpp
#define ASTNodes_hpp

#include "Application.hpp"
#include "Variable.hpp"
#include "Function.hpp"
#include "ASTNode.hpp"

#endif /* ASTNodes_hpp */
