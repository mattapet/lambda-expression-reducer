//
//  Variable.hpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/19/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#ifndef Variable_hpp
#define Variable_hpp

#include "ASTNode.hpp"

#include <string>
#include <memory>

namespace lambda {
    
    /// \brief Encapsulation of a lambda variable. Each of the variables can be
    ///  bound or free, which is deremined by an instance of \c VariableResovler
    ///  class, which scopes and assigns proper id to each variable.
    ///  A variable, which has an ID of \c 0, is free, otherwise a variable
    ///  is bound to a function with the same ID.
    class Variable: public ASTNode {
        std::string _identifier;
        std::size_t _id = 0;
        
    public:
        Variable(const std::string &identifier, std::size_t id = 0);
        
        /// Returns an identifier of the variable.
        std::string identifier() {
            return _identifier;
        }
        
        /// Sets identifier of the variable.
        void identifier(const std::string &identifier) {
            _identifier = identifier;
        }
        
        /// Returns id of the variable.
        std::size_t id() const {
            return _id;
        }
        
        /// Sets the id of the variable.
        void id(std::size_t id) {
            _id = id;
        }
        
        // MARK: - Abstract AST methods.
        
        virtual std::shared_ptr<ASTNode> clone() override;
        
        virtual void walk(ASTWalker *walker) override;
        
        virtual std::shared_ptr<ASTNode>
        eval(std::shared_ptr<Scope> scope) override;
        
        virtual std::shared_ptr<ASTNode>
        substitute(std::size_t id, std::shared_ptr<ASTNode> value) override;
    };

}


#endif /* Variable_hpp */
