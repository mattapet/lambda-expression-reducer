//
//  Function.hpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/19/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#ifndef Function_hpp
#define Function_hpp

#include "ASTNode.hpp"

#include <string>
#include <memory>

namespace lambda {
    
    /// \brief Encapsulation of a lambda function. It contains an argument as
    ///  a \c string and a body, which is an AST node. For the evaluation
    ///  purposes, every function has an \c id property, which identifies and
    ///  scopes argument variable of the function body.
    ///  Function cannot scope it's argument by itself, so to do so an instance
    ///  of \c VariableResolver class is used.
    class Function: public ASTNode {
        std::string _argument;
        std::shared_ptr<ASTNode> _body;
        std::size_t _id = 0;
        
    public:
        Function(std::string argument,
                 std::shared_ptr<ASTNode> body,
                 std::size_t id = 0);
        
        /// Returns a value of the argument.
        const std::string &argument() const {
            return _argument;
        }
        
        /// Sets the value of the arguemnt.
        void argument(const std::string &argument) {
            _argument = argument;
        }
        
        /// Returns ID of the argument variable.
        std::size_t id() const {
            return _id;
        }
        
        /// Sets ID of the argument variable.
        void id(std::size_t id) {
            _id = id;
        }
        
        /// Return the function body as a AST subtree.
        std::shared_ptr<ASTNode> body() {
            return _body;
        }
        
        // MARK: - Abstract AST methods.
        
        virtual std::shared_ptr<ASTNode> clone() override;
        
        virtual void walk(ASTWalker *walker) override;
        
        virtual std::shared_ptr<ASTNode>
        eval(std::shared_ptr<Scope> scope) override;
        
        virtual std::shared_ptr<ASTNode>
        substitute(std::size_t id, std::shared_ptr<ASTNode> value) override;
    };
}

#endif /* Function_hpp */
