//
//  Exception.cpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 12/10/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#include "Exception.hpp"

using namespace lambda;

ParseException::ParseException(const std::string &what)
: std::runtime_error(what)
{}


UnexpectedSymbolException::UnexpectedSymbolException(const SourceLocation &loc)
: ParseException("Unexpected symbol at " + loc.toString() + "."),
  _symbol(""),
  _loc(loc)
{}


UnexpectedSymbolException::UnexpectedSymbolException(const std::string &symbol,
                                                     const SourceLocation &loc)
: ParseException("Unexpected symbol `"
                 + symbol + "' at " + loc.toString()
                 + "."
                 ),
  _symbol(symbol),
  _loc(loc)
{}


UnexpectedSymbolException::UnexpectedSymbolException(char symbol,
                                                     const SourceLocation &loc)
: ParseException("Unexpected symbol `"
                 + std::string(1, symbol) + "' at " + loc.toString()
                 + "."
                 ),
_symbol(std::string(1, symbol)),
_loc(loc)
{}


const char *UnexpectedSymbolException::what() const noexcept {
    if (_symbol.length()) {
        return std::string("ParseError: Unexpected symbol `"
            + _symbol + "' at " + _loc.toString()
        + ".").c_str();
    }
    return std::string("ParseError: Unexpected symbol at "
        + _loc.toString()
    + ".").c_str();
}

ParenthesizationMismatch::ParenthesizationMismatch()

: ParseException("Parenthesization Mismatch.")
{}

const char *ParenthesizationMismatch::what() const noexcept {
    return "Parse error: Parenthesization Mismatch.";
}
