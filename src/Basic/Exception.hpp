//
//  Exception.hpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 12/10/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#ifndef Exception_hpp
#define Exception_hpp

#include "SourceLocation.hpp"

#include <string>
#include <stdexcept>

namespace lambda {
    
    class ParseException: public std::runtime_error {
    public:
        ParseException(const std::string &what);
    };
    
    
    class UnexpectedSymbolException: public ParseException {
        std::string _symbol;
        SourceLocation _loc;
        
    public:
        UnexpectedSymbolException(const SourceLocation &loc);
    
        UnexpectedSymbolException(const std::string &symbol,
                                  const SourceLocation &loc);
        
        UnexpectedSymbolException(char symbol, const SourceLocation &loc);
        virtual const char* what() const noexcept override;
    };
    
    
    class ParenthesizationMismatch: public ParseException {
    public:
        ParenthesizationMismatch();
        
        virtual const char *what() const noexcept override;
    };
    
}

#endif /* Exception_hpp */
