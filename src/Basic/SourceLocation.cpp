//
//  SourceLocation.cpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/30/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#include "SourceLocation.hpp"
#include <sstream>

using namespace lambda;

// MARK: - SourceLocation

SourceLocation::SourceLocation(std::size_t row, std::size_t col)
: line(row),
col(col)
{}

void SourceLocation::print() const {
    print(std::cout);
}

void SourceLocation::print(std::ostream &output)  const {
    output << "(" << line << "," << col << ")";
}

std::string SourceLocation::toString() const {
    std::stringstream ss;
    print(ss);
    return ss.str();
}


// MARK: - SourceRange

SourceRange::SourceRange(const SourceLocation &start,
                         const SourceLocation &end)
: start(start),
  end(end)
{}
