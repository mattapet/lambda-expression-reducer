//
//  SourceLocation.hpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/30/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#ifndef SourceLocation_hpp
#define SourceLocation_hpp

#include <iostream>

namespace lambda {
    /// \brief Struct containing a location in the source code buffer
    ///  in (row, col) fashion. The struct is immutable and does not allow
    ///  the row and col to be mutated.
    struct SourceLocation {
        const std::size_t line = 0;
        const std::size_t col = 0;
        
        /// Constructs a \c SourceLocation object.
        /// \param line Line number of the location.
        /// \param col Column number of the location.
        SourceLocation(std::size_t line, std::size_t col);
        
        /// Prints out current \c SourceLocation object to a standart output.
        /// The ouput will be formatted: \c ( \c line \c , \c column \c ).
        void print() const;
        
        /// Prints out current \c SourceLocation object to a provided output
        /// stream.
        /// The ouput will be formatted: \c ( \c line \c , \c column \c ).
        void print(std::ostream &output) const;
        
        /// Transforms current \c SourceLocation object to a string and returns
        /// it.
        /// The ouput will be formatted: \c ( \c line \c , \c column \c ).
        std::string toString() const;
    };
    
    /// \brief Struct containing start and end locations of encapsulated text
    ///  in the source buffer. The struct is immutable and does not allow
    ///  to change start nor the end of the source location.
    struct SourceRange {
        const SourceLocation start;
        const SourceLocation end;
        
        SourceRange(const SourceLocation &start, const SourceLocation &end);
    };

}

#endif /* SourceLocation_hpp */
