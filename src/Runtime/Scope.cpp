//
//  Scope.cpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 12/2/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#include "Scope.hpp"

using namespace lambda;

void Scope::push(const std::size_t &id, std::shared_ptr<ASTNode> value) {
    if (_values.find(id) == _values.end()) {
        std::stack<std::shared_ptr<ASTNode>> stack;
        stack.push(value);
        _values.insert({id, std::move(stack)});
        
    } else {
        _values.at(id).push(value);
    }
}

void Scope::pop(const std::size_t &id) {
    _values.at(id).pop();
}

bool Scope::hasValue(std::size_t id) const {
    if (_values.find(id) != _values.end()) {
        return !_values.at(id).empty();
    } else {
        return false;
    }
}

std::shared_ptr<ASTNode>
Scope::lookupValueForIdentifier(const std::size_t &id) {
    if (_values.find(id) != _values.end()) {
        auto &stack = _values.at(id);
        return stack.size() ? stack.top() : nullptr;
        
    } else {
        return nullptr;
    }
}


