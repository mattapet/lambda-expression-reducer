//
//  VariableResolver.cpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 12/2/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#include "VariableResolver.hpp"

using namespace lambda;

// MARK: - Private implementation

std::size_t VariableResolver::generateId() {
    return ++_id;
}

bool VariableResolver::containsId(const std::string &arg) const {
    if (_ids.find(arg) != _ids.end()) {
        return !_ids.at(arg).empty();
    } else {
        return false;
    }
}

void VariableResolver::pushId(const std::string &arg, std::size_t id) {
    if (_ids.find(arg) == _ids.end()) {
        std::stack<std::size_t> stack;
        stack.push(id);
        _ids.insert({arg, std::move(stack)});
        
    } else {
        _ids.at(arg).push(id);
    }
}

void VariableResolver::popId(const std::string &arg) {
    _ids.at(arg).pop();
}

// MARK: - Public implementation

void VariableResolver::resolve(std::shared_ptr<ASTNode> ast) {
    ast->walk(this);
}

void VariableResolver::walk(Application *app) {
    app->left()->walk(this);
    app->right()->walk(this);
}

void VariableResolver::walk(Function *func) {
    auto id = generateId();
    pushId(func->argument(), id);
    func->id(id);
    func->body()->walk(this);
    popId(func->argument());
}


void VariableResolver::walk(Variable *var) {
    if (containsId(var->identifier())) {
        var->id(_ids.at(var->identifier()).top());
    }
}


