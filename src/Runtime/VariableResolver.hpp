//
//  VariableResolver.hpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 12/2/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#ifndef VariableResolver_hpp
#define VariableResolver_hpp

#include "../AST/ASTWalker.hpp"
#include "../AST/ASTNodes.hpp"

#include <string>
#include <stack>
#include <unordered_set>
#include <unordered_map>
#include <iostream>

namespace lambda {
    
    /// \brief A \b mutating \c AST walker, that walkes throught the whole AST
    ///  and substitutes variables and parameters with colliding identifiers
    ///  with new, unique ones.
    ///
    ///  \description The \c VariableResoler objects determine whether a
    ///  variable is \b bound or \b free. To print out information about
    ///  the variables, pass \c true to the constructor of the
    ///  \c VariableResolver object.
    ///
    ///  The \c VariableResolver does not do any complicated computing, but it
    ///  still has to iterate over the whole \c AST structure. Therefore it is
    ///  bound by \i O(n), where the \i n is number of the \c AST nodes in the
    ///  \c AST.
    class VariableResolver: public ASTWalker {
        std::unordered_map<
          std::string,
          std::stack<std::size_t>
        > _ids;
        std::size_t _id = 0;
        
    private:
        std::size_t generateId();
        bool containsId(const std::string &arg) const;
        void pushId(const std::string &arg, std::size_t id);
        void popId(const std::string &arg);
        
    public:
        VariableResolver() = default;
        
        void resolve(std::shared_ptr<ASTNode> ast);

        virtual void walk(Application *app) override;
        virtual void walk(Function *func) override;
        virtual void walk(Variable *var) override;
        
    };
    
}

#endif /* VariableResolver_hpp */
