//
//  Scope.hpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 12/2/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#ifndef Scope_hpp
#define Scope_hpp

#include <stack>
#include <string>
#include <unordered_map>
#include <memory>

namespace lambda {
    
    class ASTNode;
    
    /// \brief Encapsulates all currently scoped variables.
    class Scope {
        /// \brief A dictionary containing pairs of identifiers and stacks
        ///  of values associated with them.
        std::unordered_map<
          std::size_t,
          std::stack<std::shared_ptr<ASTNode>>
        > _values;
        
        /// \brief Last provided
        std::shared_ptr<ASTNode> _parameter;
        
        bool _didPerformBetaReduction = false;
        
    public:
        Scope() = default;
        ~Scope() = default;
        
        /// \brief Returns last provided parameter.
        std::shared_ptr<ASTNode> parameter() {
            auto ret = _parameter;
            _parameter = nullptr;
            return ret;
        }
        
        /// \brief Sets parameters.
        void parameter(std::shared_ptr<ASTNode> param) {
            _parameter = param;
        }
        
        bool didPerformBetaReduction() const {
            return _didPerformBetaReduction;
        }
        
        void performedBetaReduction() {
            _didPerformBetaReduction = true;
        }
        
        /// \brief Pushes a value for given identifier to the value stack.
        void push(const std::size_t &id, std::shared_ptr<ASTNode> value);
        /// \brief Pops a value from given identifier stack.
        void pop(const std::size_t &id);
        
        bool hasValue(std::size_t id) const;
        
        /// \brief Returns value for identifier, if such variable has been
        ///  pushed to the current scope, otherwise returns \c nullptr.
        std::shared_ptr<ASTNode>
        lookupValueForIdentifier(const std::size_t &id);
    };
    
}

#endif /* Scope_hpp */
