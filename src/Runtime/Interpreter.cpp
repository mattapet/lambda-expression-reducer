//
//  Interpreter.cpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 12/2/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#include "Interpreter.hpp"

using namespace lambda;

std::shared_ptr<ASTNode> Interpreter::interpret(std::shared_ptr<ASTNode> ast) {
    std::shared_ptr<Scope> scope = nullptr;
    VariableResolver resolver;
    do {
        scope = std::make_shared<Scope>();
        resolver.resolve(ast);
        ast = ast->eval(scope);
    } while (scope->didPerformBetaReduction());
    return ast;
}


// MARK: - ASTNode evaluation

std::shared_ptr<ASTNode> Application::eval(std::shared_ptr<Scope> scope)  {
    if (auto func = std::dynamic_pointer_cast<Function>(_left)) {
        scope->performedBetaReduction();
        return func->body()->substitute(func->id(), _right);
        
    } else {
        auto left = _left->eval(scope);
        auto right = _right->eval(scope);
        if (left && right) {
            return std::make_shared<Application>(left, right);
        } else if (!right) {
            return left;
        } else if (!left) {
            return right;
        } else {
            return nullptr;
        }
    }
}

std::shared_ptr<ASTNode> Function::eval(std::shared_ptr<Scope> scope)  {
    return std::make_shared<Function>(
        _argument,
        _body->eval(scope),
        _id
    );
}

std::shared_ptr<ASTNode> Variable::eval(std::shared_ptr<Scope> scope)  {
    return clone();
}

// MARK: - AST substitutions

std::shared_ptr<ASTNode>
Application::substitute(std::size_t id, std::shared_ptr<ASTNode> value)  {
    return std::make_shared<Application>(
        _left->substitute(id, value),
        _right->substitute(id, value)
    );
}

std::shared_ptr<ASTNode>
Function::substitute(std::size_t id, std::shared_ptr<ASTNode> value)  {
    return std::make_shared<Function>(
        _argument,
        _body->substitute(id, value),
        _id
    );
}

std::shared_ptr<ASTNode>
Variable::substitute(std::size_t id, std::shared_ptr<ASTNode> value)  {
    return _id == id ? value->clone() : clone();
}
