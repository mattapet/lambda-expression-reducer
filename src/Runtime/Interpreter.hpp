//
//  Interpreter.hpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 12/2/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#ifndef Interpreter_hpp
#define Interpreter_hpp

#include "../AST/ASTNodes.hpp"
#include "Scope.hpp"
#include "VariableResolver.hpp"
#include "Printer.hpp"

#include <stack>
#include <memory>

namespace lambda {
    
    /// \brief A class, whose objects are able to evaluate lambda expressions.
    /// \description Interpreter uses \c call-by-name evaluation method, which
    ///  means, the evaluation itself is fairly slow (factorial of five takes
    ///  abount 40 seconds to evalue).
    ///
    /// \see https://en.wikipedia.org/wiki/Evaluation_strategy
    class Interpreter {
    public:
        Interpreter() = default;
        virtual ~Interpreter() = default;
        
        /// Evaluates a given expression in form of a AST.
        std::shared_ptr<ASTNode> interpret(std::shared_ptr<ASTNode> ast);
    };
    
}

#endif /* Interpreter_hpp */
