//
//  Printer.hpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 12/2/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#ifndef Printer_hpp
#define Printer_hpp

#include "../AST/ASTWalker.hpp"
#include "../AST/ASTNodes.hpp"
#include <iostream>
#include <sstream>

namespace lambda {
    
    /// \brief Class which instances are able to output a valid lambda ASTs
    ///  into provided output stream. The output expressions are fully
    ///  parenthesized.
    class Printer: public ASTWalker {
        std::ostream &_output;
        bool _ids = false;
        
    protected:
        virtual void didEnter(Application *app) override;
        virtual void willLeave(Application *app) override;
        virtual void didEnter(Function *func) override;
        virtual void willLeave(Function *func) override;
        virtual void didEnter(Variable *var) override;
        
    public:
        Printer(std::ostream &output = std::cout, bool ids = false);
        
        void print(ASTNode *expr) {
            expr->walk(this);
        }
        
        void print(std::shared_ptr<ASTNode> expr) {
            expr->walk(this);
        }
        
        virtual void walk(Application *app) override;        
    };
    
}


#endif /* Printer_hpp */
