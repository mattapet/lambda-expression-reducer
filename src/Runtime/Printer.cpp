//
//  Printer.cpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 12/2/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#include "Printer.hpp"

using namespace lambda;

Printer::Printer(std::ostream &output, bool ids)
: _output(output),
  _ids(ids)
{}

// MARK: - Protected implementation


void Printer::didEnter(Application *app) {
    _output << "(";
}

void Printer::willLeave(Application *app) {
    _output << ")";
}

void Printer::didEnter(Function *func) {
    if (_ids) {
        _output << "(lambda " << func->id() << " . ";
    } else {
        _output << "(lambda " << func->argument() << " . ";
    }
    
}

void Printer::willLeave(Function *func) {
    _output << ")";
}

void Printer::didEnter(Variable *var) {
    if (_ids) {
        _output << var->id();
    } else {
        _output << var->identifier();
    }
}

// MARK: - Public implementation

void Printer::walk(Application *app) {
    didEnter(app);
    if (app->left()) {
        app->left()->walk(this);
    }
    _output << (app->left() && app->right() ? " " : "");
    if (app->right()) {
        app->right()->walk(this);
    }
    willLeave(app);
}
