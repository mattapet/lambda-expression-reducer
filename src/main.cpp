//
//  main.cpp
//  Lambda expression reducer
//
//  Created by Peter Matta on 11/19/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

#include "AST/AST.hpp"
#include "Basic/Exception.hpp"
#include "Runtime/Printer.hpp"
#include "Runtime/Interpreter.hpp"
#include "Parse/Parser.hpp"
#include <iostream>

using namespace lambda;

int main(int argc, const char * argv[]) {
    try {
        Parser parser(std::cin);
        Printer printer(std::cout);
        Interpreter interpreter;
        
        auto ast = parser.parse();
        auto result = interpreter.interpret(ast);
        printer.print(result);
        std::cout << std::endl;

    } catch (ParseException &e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    
    return 0;
}
