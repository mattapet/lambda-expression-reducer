# Project name
TARGET = mattapet

# directories
CURDIR   = .
SRCROOT  = $(CURDIR)
SRCDIR   = $(SRCROOT)/src
BUILDDIR = $(SRCROOT)/build
INCLUDE  = $(SRCROOT)/src
LIB      = $(SRCROOT)/lib
BINDIR   = $(SRCROOT)/bin

MAKESUBDIR = $(SRCDIR)

# Compilers
CC  = gcc
CXX = g++
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
	CXXFLAGS = -std=c++1z -Wall -pedantic -Wno-long-long -O2 -I$(INCLUDE)
else
	CXXFLAGS = -std=c++17 -Wall -pedantic -Wno-long-long -O2 -I$(INCLUDE)
endif
LFLAGS = -Wall -pedantic

# Files
SRCS := $(shell find $(SRCDIR)  -name '*.cpp')
OBJS := $(subst .cpp,.o,$(SRCS))

all: clean compile

run: $(TARGET)
	./$(TARGET)

$(TARGET): compile

compile: $(OBJS)
	$(CXX) $(LFLAGS) $(OBJS) -o $(TARGET)

depend: .depend

.depend: $(SRCS)
	$(RM) ./.depend
	$(CXX) $(CXXFLAGS) -MM $^ >> ./.depend;

clean:
	-rm -rf doc
	$(RM) *~ .depend
	$(RM) $(OBJS)
	$(RM) $(TARGET)

include .depend

.PHONY: doc

doc:
	doxygen Doxyfile
